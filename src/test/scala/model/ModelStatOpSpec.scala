package model

import org.scalatest._

class ModelStatOpSpec extends FlatSpec with Matchers {

  implicit val sampling = new DoubleInference

  "Model" should "calculate expected value in confidence interval" in {
    val a = new Constant(5)
    a.expectedVal.mean should be === 5

    val b = new Gaussian(0, 1)
    b.expectedVal.mean should be < 0.1

    val flip = new Flip(0.7)
    val expectedFlip = flip.expectedVal
    expectedFlip.mean should be > 0.6
    expectedFlip.mean should be < 0.8
    println("Mean of sampled expected value for flip 0.7: " + expectedFlip.mean + " -+ " + expectedFlip.interval)

    // random between 0 and 4 inclusive
    val c = new CustomRandom(() => Sample(Math.floor(Math.random() * 5), 0.2))
    val expectedC = c.expectedVal
    expectedC.mean should be >= 2 - expectedC.interval
    expectedC.mean should be <= 2 + expectedC.interval
    println("Mean of sampled expected value for c: " + expectedC.mean + " -+ " + expectedC.interval)

    // random between 0 and 9 inclusive
    val d = new CustomRandom(() => Sample(Math.floor(Math.random() * 10), 0.1))
    val expectedD = d.expectedVal
    expectedD.mean should be >= 4.5 - expectedD.interval
    expectedD.mean should be <= 4.5 + expectedD.interval
    println("Mean of sampled expected value for d: " + expectedD.mean + " -+ " + expectedD.interval)

    val e = for {
      cSample <- c
      dSample <- d
    } yield cSample + dSample

    val expectedE = e.expectedVal
    expectedE.mean should be >= 6.5 - expectedE.interval
    expectedE.mean should be <= 6.5 + expectedE.interval
    println("Mean of sampled expected value for e: " + expectedE.mean + " -+ " + expectedE.interval)

    val f = for {
      cSample <- c
      dSample <- d
    } yield dSample - cSample

    val expectedF = f.expectedVal
    expectedF.mean should be >= 2.5 - expectedF.interval
    expectedF.mean should be <= 2.5 + expectedF.interval
    println("Mean of sampled expected value for f: " + expectedF.mean + " -+ " + expectedF.interval)
  }

  "Model" should "compare two probabilities" in {
    val a = new Constant(5)
    val b = new Constant(2)
    //(a > b).expectedVal.mean should be === 1
    println("Mean of (a > b).expectedVal is 1 ")


    val c = new Gaussian(10, 1)
    val d = new Gaussian(0, 1)
    //(c > d).expectedVal.mean should be === 1
    println("Mean of (c > d).expectedVal is 1 ")


    // The mean is 0.9985369457862211 for Gaussians with means 3 and 0
    val e = new Gaussian(4, 1)
    val f = new Gaussian(0, 1)
    /*val efE = (e > f).expectedVal
    efE.mean should be >= 1 - efE.interval
    efE.mean should be <= 1 + efE.interval
    println("Mean of (e > f).expectedVal is: " + efE.mean + " -+ " + efE.interval)*/

  }

  "Model" should "make inference given samples" in {
    val dist = sampling.inferenceBySamples(List(Sample[Double](1, 0.2), Sample[Double](2, 0.5), Sample[Double](3, 0.1), Sample[Double](1, 0.2), Sample[Double](2, 0.5)))
    dist(1) should be === (0.4 / 1.5)
    dist(2) should be === (1 / 1.5)
    dist(3) should be === (0.1 / 1.5)
  }

  "Model" should " operate with for convention and calculate distribution operations" in {
    val aAndB = for {
      a <- new Flip(0.5)
      b <- new Flip(0.5)
    } yield a && b

    val result = aAndB.expectedVal

    result.mean should be >= 0.25 - result.interval
    result.mean should be <= 0.25 + result.interval
    println("a and b in Flip(0.5). \n Mean of (a && b).expectedVal is: " + result.mean + " -+ " + result.interval)


    val cAndD = for {
      c <- new Flip(0.5)
      if (c)
      d <- new Flip(0.5)
    } yield c && d

    val result2 = cAndD.expectedVal
    result2.mean should be >= 0.5 - result2.interval
    result2.mean should be <= 0.5 + result2.interval
    println("c and d in Flip(0.5) and c is true \n Mean of (c && d).expectedVal is: " + result2.mean + " -+ " + result2.interval)


    val ePlusf = for {
      e <- new Gaussian(1, 0.5)
      f <- new Gaussian(5, 0.5)
    } yield e + f


    val result3 = ePlusf.expectedVal

    result3.mean should be >= 6 - result3.interval
    result3.mean should be <= 6 + result3.interval
    println("c and d in Flip(0.5) and c is true \n Mean of (e + d).expectedVal is: " + result3.mean + " -+ " + result3.interval)

    // (new Gaussian).map(a => a)
    // (new Gaussian).flatMap(a => new Gaussian().map(b => a + b))

    // TODO: How to constrain both a and b ? e.g. if(a \\ b)   Will it only resample b???
  }

  "Model" should "make inference to calculate the probability of Pearl's burglary example" in {

    val burglaryModel = for {
      earthquake <- new Flip(0.001)
      burglary <- new Flip(0.01)
      alarm = earthquake || burglary
      phoneWorking <- if (earthquake) new Flip(0.6) else new Flip(0.99)
      //if phoneWorking
      maryWakes <- if (alarm && earthquake) new Flip(0.8) else if (alarm) new Flip(0.6) else new Flip(0.2)
      // if maryWakes
      //if maryWakes && phoneWorking
    } yield (phoneWorking, maryWakes, burglary)

    val resultModel = for {
      (p, m, b) <- burglaryModel
      if p && m // called = marryWakes && phoneWorking
    } yield b

    val result = resultModel.expectedVal

    println("The probability of burglary is: " + result.mean + " -+ " + result.interval)
    val burglaryLikely = sampling.Pr[Boolean](resultModel, p => p)
    println("Is burglary likely? " + burglaryLikely + "\n")
  }

  "Model" should "make inference to calculate probabilities in Monty Hall problem" in {

    val montyHall = {
      val doors = (1 to 3).toSet
      for {
        prize <- new DiscreteUniform(doors) // The prize is placed randomly
        choice <- new DiscreteUniform(doors) // You choose randomly
        opened <- new DiscreteUniform(doors - prize - choice) // Monty opens one of the other doors
        switch <- new DiscreteUniform(doors - choice - opened) // You switch to the unopened door
      } yield (prize, switch)
    }

    val a = sampling.Pr[(Int, Int)](montyHall, p => p._1 == p._2, 0.60)
    a shouldBe true
  }

}


