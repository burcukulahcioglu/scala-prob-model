package model

class Flip(prob: Double) extends Model[Boolean] {

  implicit def double2Bool(b: Boolean) = b

  override def sample: Sample[Boolean] = {
    Sample(Math.random() < prob, prob)
  }
}
