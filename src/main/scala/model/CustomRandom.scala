package model

/**
  *  Model whose distribution is not known, we only have a sampling function
  */
class CustomRandom[T](getSamplerOp: () => Sample[T]) extends Model[T] {
  def sample() = getSamplerOp()
}
