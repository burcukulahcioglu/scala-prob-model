package model

import scala.annotation.tailrec

abstract class Model[T] {

  def sample: Sample[T]

  def expectedVal(implicit sampling: Inference[Double]): SampleMean = {
    val numSamples = sampling.sampleSize()
    val samples = (1 to numSamples).map(i => sample).toList

    // return the sample mean and the confidence interval
    // Confidence interval value for 95% confidence is 1.96
    SampleMean(sampling.weightedMean(samples), 1.96 * sampling.weightedStdDev(samples) / Math.sqrt(numSamples))
  }

  def inference(implicit sampling: Inference[Double]): Distribution[T] = {
    val numSamples = sampling.sampleSize()
    val samples = (1 to numSamples).map(i => sample).toList
    sampling.inferenceBySamples(samples)
  }

  // sample'i boyle olan yeni bir dist olustur, hesaplama hemen
  def flatMap[R](f: T => Model[R]): Model[R] = new Model[R] {
    override def sample = {
      val thisSample = Model.this.sample
      f(thisSample.value).sample
    }
  }

  def map[R](f: T => R): Model[R] = new Model[R] {
    override def sample = {
      val thisSample = Model.this.sample
      Sample(f(thisSample.value), thisSample.prob)
    }
  }

  def filter(p: T => Boolean) = new Model[T] {
    @tailrec
    override def sample = {
      val thisSample = Model.this.sample
      if (p(thisSample.value)) thisSample else this.sample
    }
  }
}

