package model

import scala.math.Numeric

/**
  * Has operations for on Model[T] and Sample[T] where T can be represented as Double
  */
trait RealInference extends Inference[Double] {

  def toDouble[T](t: T): Double

  override def weightedMean[T](samples: List[Sample[T]]): Double =
    samples.map(s => toDouble(s.value) * s.prob).sum / sumOfProb(samples)

  override def weightedStdDev[T](samples: List[Sample[T]]): Double = {
    val sumOfProbVal = sumOfProb(samples)
    val weightedMeanVal = weightedMean(samples)
    val variance = samples.map(s => s.prob * Math.pow(toDouble(s.value) - weightedMeanVal, 2)).sum /
      (sumOfProbVal - (sumOfProbSquares(samples) / sumOfProbVal))
    Math.sqrt(variance)
  }
}

class DoubleInference extends RealInference {

  override def toDouble[T](t: T): Double = t match {
    case d: Double => d
    case i: Int => i.toDouble
    case f: Float => f.toDouble
    case b: Boolean => if (b) 1.0 else 0.0
  }
}