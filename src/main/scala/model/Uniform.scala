package model

import scala.util.Random

/**
  * Created by burcuozkan on 15/03/16.
  */
class DiscreteUniform(values: Set[Int]) extends Model[Int]{
  override def sample: Sample[Int] = {
    Sample[Int](values.toList(Random.nextInt(values.size)), 1.0/values.size)
  }
}
