package model

case class Sample[T](value: T, prob: Double)

case class SampleMean(mean: Double, interval: Double) {
  def isAround(d: Double): Boolean = {
    if(d < (mean + interval) && d > (mean - interval)) true else false
  }
}

