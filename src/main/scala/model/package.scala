package object model {

  type Distribution[T] = Map[T, Double]
}
