package model

/* Integer and Double constants can be lifted to Model objects */
class Constant(val value: Double) extends Gaussian(value, 0) {

  override def sample: Sample[Double] = Sample(value, 1)

  override def expectedVal(implicit sampling: Inference[Double]): SampleMean = SampleMean(value, 0)
}

object Constant {
  def apply(v: Int) = new Constant(v)
}