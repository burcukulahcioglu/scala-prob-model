package model

/**
  * Contains generic sampling operations
  */
trait Inference[R] {

  val SAMPLE_SIZE = 1000

  val MAX_SAMPLE_SIZE = 10000
  val INITIAL_SAMPLE_SIZE = 100
  val SAMPLE_SIZE_STEP = 100

  /**
    * required number of samples to have confidenceLevel confidence in the epsilon interval for sample mean
    * (i.e. 95% confidence for calculatedMean +- epsilon)
    */
  def sampleSize(epsilon: Double = 0.5, confidenceLevel: Double = 0.95): Int = {
    //Math.ceil((2 + epsilon) / (epsilon * epsilon) * Math.log(2 / (1 - confidenceLevel))).round.toInt
    // TODO implement sample size
    SAMPLE_SIZE
  }

  def checkHypothesis[T](m: Model[T], pred: T => Boolean, h0: Double, h1: Double, A: Double, B: Double): Boolean = {

    def collectSamplesAndCheck(numSamples: Int, acceptedWeightOld: Double, allWeightOld: Double): Boolean = {
      val samples = (1 to SAMPLE_SIZE_STEP).map(i => m.sample)
      val accepted = samples.filter(s => pred(s.value))
      val acceptedWeight = acceptedWeightOld + sumOfProb(accepted.toList)
      val allWeight = allWeightOld + sumOfProb(samples.toList)

      val logLikelihood = acceptedWeight * Math.log(h1 / h0) + (allWeight - acceptedWeight) * Math.log((1 - h1) / (1 - h0))

      if (logLikelihood >= B)
        true
      else if (logLikelihood <= A)
        false
      else if (numSamples < MAX_SAMPLE_SIZE)
        collectSamplesAndCheck(numSamples + SAMPLE_SIZE_STEP, acceptedWeight, allWeight)
      else
        false
    }

    collectSamplesAndCheck(0, 0, 0)
  }

  // not weighted
  def checkHypothesis2[T](m: Model[T], pred: T => Boolean, h0: Double, h1: Double, A: Double, B: Double): Boolean = {

    def collectSamplesAndCheck2(numAllSamples: Int, numAcceptedSamples: Int): Boolean = {
      val samples = (1 to SAMPLE_SIZE_STEP).map(i => m.sample)
      val accepted = samples.count(s => pred(s.value))
      val acceptedAll = accepted + numAcceptedSamples

      val logLikelihood = (acceptedAll * Math.log(h1 / h0)) + ((numAllSamples + SAMPLE_SIZE_STEP - acceptedAll) * Math.log((1 - h1) / (1 - h0)))

      if (logLikelihood >= B)
        true
      else if (logLikelihood <= A)
        false
      else if (numAllSamples < MAX_SAMPLE_SIZE) {
        collectSamplesAndCheck2(numAllSamples + SAMPLE_SIZE_STEP, acceptedAll)
      }
      else
        false
    }

    collectSamplesAndCheck2(0, 0)
  }

  // True if this Bernoulli is true with probability at least prob
  def Pr[T](m: Model[T], pred: T => Boolean, prob: Double = 0.5, alpha: Double = 0.05, epsilon: Double = 0.03,
            maxSampleSize: Int = MAX_SAMPLE_SIZE, initSampleSize: Int = INITIAL_SAMPLE_SIZE, sampleSizeStep: Int = SAMPLE_SIZE_STEP): Boolean = {

    // The hypotheses being compared
    val h0 = prob - epsilon // H_0 : p <= prob - epsilon
    val h1 = prob + epsilon // H_1 : p >= prob + epsilon

    // Decide the log-likelihood thresholds for the test
    val beta = alpha
    // We are symmetric w.r.t. false positives/negatives
    val A = Math.log(beta / (1 - alpha))
    // Accept H_0 if the log-likelihood is <= a
    val B = Math.log((1 - beta) / alpha) // Accept H_1 if the log-likelihood is >= b

    checkHypothesis2(m, pred, h0, h1, A, B)
  }

  /**
    * e.g. samples: Sample(1, 0.2), Sample(2, 0.5), Sample(3, 0.1), Sample(1, 0.2), Sample(2, 0.5))
    * groupedSamples: ((1, 0.4), (2, 1), (3, 0.1))
    * dist: ((1, 0.4/1.5), (2, 1/1.5), (c, 0.1/1.5))
    */
  def inferenceBySamples[T](samples: List[Sample[T]]): Distribution[T] = {
    val groupedSamples = samples.groupBy(s => s.value).map(g => (g._1, sumOfProb(g._2)))
    val weightSum = groupedSamples.values.sum
    groupedSamples.map(p => p._1 -> p._2 / weightSum)
  }

  def sumOfProb[T](samples: List[Sample[T]]): Double = samples.map(_.prob).sum

  def sumOfProbSquares[T](samples: List[Sample[T]]): Double = samples.map(s => Math.pow(s.prob, 2)).sum

  def weightedMean[T](samples: List[Sample[T]]): R

  def weightedStdDev[T](samples: List[Sample[T]]): R
}
