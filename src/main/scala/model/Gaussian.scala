package model

import scala.util.Random

class Gaussian(var mu: Double, var sigma: Double) extends Model[Double]  {

  override def sample: Sample[Double] = {
    val sValue = Random.nextGaussian() * sigma + mu
    val prob = probOf(sValue)
    Sample(sValue, prob)
  }

  // f(x | mu, variance)
  def probOf(x: Double): Double = {
    (Math.exp(-1 * Math.pow(x - mu, 2) / (2 * Math.pow(sigma, 2)))) /
      (sigma * Math.sqrt(2 * Math.PI))
  }
}
